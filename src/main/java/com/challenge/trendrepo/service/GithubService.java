package com.challenge.trendrepo.service;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.Stream;
 
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.trendrepo.model.RepositoryData;
import com.challenge.trendrepo.model.ResponseData; 
import com.challenge.trendrepo.ressourceResolver.WebApiResolver;





@Service
public class GithubService {
	
	@Autowired
	private WebApiResolver webApiResolver; 
	
	
	/**
	 * Returns an Stream<List<ResponseData>> that stream an List of 
	 * ResponseData (Language, count of repositories that uses this 
	 * language and repositories that uses this language (Top 100 trends 
	 * repositories)
	 * 
	 * 
	 *  @param fromDate Date begin
	 *  @return Stream
	 *
	 */
	
	@SuppressWarnings("rawtypes")
	public Stream  getResult$(Date fromDate) {
		
		
		return webApiResolver
		.getRepos$(fromDate) // Returns Root object ( containt List of RepositoryData
			.map(resp->resp.getItems()) // Retrive RepositoryData List
			.map( items -> { 
				return items // Return Streams Of ResponseData
							.stream() // Convert RepositoryData List to Sequential Stream
							.map(item->{ 
								return webApiResolver 
										.getlanguages$(item.getLanguages_url()) // For each RepositoryData item Retrive used languages in the repository
										.map(resp->{
											
											/*
											 * 
											 * If no main language defined for the Repository add undefined to Languages Set
											 * 
											 */ 
											if(item.getLanguage() == null) item.setLanguage("undefined"); 
											item.addLanguage(item.getLanguage());
											
											/*
											 * 
											 * Convert Response Json to JsonObject 
											 * 
											 */ 
											JSONObject jsonObject = new JSONObject(resp);
											
											// Convert Json object Keys to Set<String> and set them in languages Set in RespositoryData 
								  			jsonObject.keys().forEachRemaining(lang ->{
								  				if(!item.getLanguages().contains(lang)) item.addLanguage(lang);
								  			});  
								  			
								  			
								  			/*
								  			 * 
								  			 * If respository had more than two languages, remove undefined from Set
								  			 * 
								  			 */  
								  			if(item.getLanguages().size() > 1) {
								  				item.getLanguages().remove("undefined");
								  			} 
											  
											return item;
								});
							}); 
						}).blockFirst()
						.map(item ->{  
							
							// Debug .....
							item.toIterable().forEach(el->{
								 System.out.println("==========================================");
								System.out.println(el.getId()+" "+ el.getLanguage());
								 System.out.println("==========================================");
							});
							
							/*
							 * 
							 * Returns Mono<List<ResponseData>>
							 * 
							 */  
							return item
									/*
									 *	GroupingBy Language
									 *
									 */
									.collect(Collectors.groupingBy(RepositoryData::getLanguage))
									.map(el->{ 
										/*
										 * 
										 * Convert each (language, List<RepositoryData ) to List<ResponseData>
										 * 
										 */
										List<ResponseData> responseDatas = new ArrayList<ResponseData>(); 
										for(String key : el.keySet()) {
											ResponseData responseData = new ResponseData();
											responseData.setLanguage(key);
											responseData.setCount(el.get(key).size());
											responseData.setRepositories(el.get(key));
											responseDatas.add(responseData);
											
											System.out.println("************************ "+key);
											System.out.println("*********** "+el.get(key).size());
										}
										return responseDatas;
									});	
							
						}); 
	}
	

		
		
	 
	
}
