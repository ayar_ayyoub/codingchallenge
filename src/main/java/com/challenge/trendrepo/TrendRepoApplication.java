package com.challenge.trendrepo;
 
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 
 

@SpringBootApplication
public class TrendRepoApplication {
	 
	
	public static void main(String[] args) {
		
		SpringApplication.run(TrendRepoApplication.class, args);
	}
	 

}
