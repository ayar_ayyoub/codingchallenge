package com.challenge.trendrepo.web.api.rest;
 
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar; 
import java.util.stream.Stream; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import com.challenge.trendrepo.service.GithubService; 
/**
 * 
 * 
 * @author AYAR Ayyoub
 * @category RestController
 * 
 * GithubRest Ressource 
 *  @URL  /github/languages/api
 *  
 */
@RestController
@RequestMapping("/github/languages/api")
public class GithubRestApi {

	
	@Autowired
	private GithubService githubService;
	
	/**
	 * 
	 * @Endpoint /languages
	 * 
	 * @return Stream<List<ResponceData> streams of Used languages for top 100 most 
	 * starred repos created the last 30 days
	 * 
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/languages")
	public Stream getUsedLanguages() {
		
		 Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -30);
		Date fromDate = cal.getTime();
		
		
		return githubService.getResult$(fromDate);
				
	}
	
	
	
	
	
	
}
