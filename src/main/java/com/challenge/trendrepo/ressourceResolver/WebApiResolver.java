package com.challenge.trendrepo.ressourceResolver;
 
import java.text.SimpleDateFormat;
import java.util.Date;
 
import org.springframework.beans.factory.annotation.Value; 
import org.springframework.stereotype.Component; 
import org.springframework.web.reactive.function.client.WebClient;
 
import com.challenge.trendrepo.model.Root;

import reactor.core.publisher.Flux;

/**
 * 
 * 
 * @author AYAR Ayyoub
 * 
 * Resolve APIs ressouces
 * @category Component
 *
 */

@Component
public class WebApiResolver {
	 
	/*
	 * 
	 * ressource.github... from application.properties
	 * 
	 */
	
	@Value("${ressource.github.api}")
	private String url;
	@Value("${ressource.github.function}")
	private String function; 
	@Value("${ressource.github.sort}")
	private String sort;
	@Value("${ressource.github.order}")
	private String order;
	@Value("${dates.pattern}")
	private String datePattern;
	@Value("${ressource.github.maxperpage}")
	private String maxPerPage;
	
	/** 
	 * Get Top 100  most starred repos created in the last from date 
	 * 
	 * @param from Beginning date
	 * @return Flux<Root> Streams of Root element
	 * 
	 */
	public Flux<Root> getRepos$(Date from) {  
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);   
		
		String ressourceURL = 
				url+
				function+
				"?q=created:>"+dateFormat.format(from)+
				"&sort="+sort+
				"&order="+order+
				"?per_page="+maxPerPage;
		/*
		 * Create WebClient and GET data from ressourceURL. then convert body to FLux<Root> object
		 */
		return  WebClient.create().get().uri(ressourceURL).retrieve().bodyToFlux(Root.class);
		
	}
	
	/**
	 * Return Languages from ressourceURL
	 * @param languages_url ressourceURL
	 * @return Flux<String> Flux Streams of languages
	 * 
	 * 
	 * 
	 */
	
public Flux<String> getlanguages$(String languages_url) {  
		   
		 
		return  WebClient.create().get().uri(languages_url).retrieve().bodyToFlux(String.class);
		
	}
	

}
