package com.challenge.trendrepo.ressourceResolver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration; 
import org.springframework.web.client.RestTemplate;


/**
 * 
 * Setting  Resttemplate Configuration Beans
 * 
 * @category Configuration
 */
@Configuration
public class RestTemplateConfiguration {

	
	/**
	 * 
	 * Bean for defining RestTemplate Object
	 * Scope : Singleton
	 * 
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
}
