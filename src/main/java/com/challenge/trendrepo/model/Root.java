package com.challenge.trendrepo.model
;

import java.io.Serializable; 
import java.util.List; 


public class Root implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1756507869124163256L;
	private String total_count; 
	private List<RepositoryData> items;
	
	public Root() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getTotal_count() {
		return total_count;
	}


	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}


	public List<RepositoryData> getItems() {
		return items;
	}

	 
	
	public void setItems(List<RepositoryData> items) {
		this.items = items;
	}


	
	
}
