package com.challenge.trendrepo.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ResponseData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5470697118042654982L;
	private String language;
	private List<RepositoryData> repositories;
	private int count;
	
}
