package com.challenge.trendrepo.model;

import java.io.Serializable; 
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RepositoryData implements Serializable, Cloneable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7757594510328703449L;

	private int id;
    
    private Set<String> languages;

    private String node_id;

    private String name;

    private String full_name; 

    private Owner owner;

    private String html_url;

    private String description;

    private boolean fork;

    private String url;

    private String forks_url;

    private String keys_url;

    private String collaborators_url;

    private String teams_url;

    private String hooks_url;

    private String issue_events_url;

    private String events_url;

    private String assignees_url;

    private String branches_url;

    private String tags_url;

    private String blobs_url;

    private String git_tags_url;

    private String git_refs_url;

    private String trees_url;

    private String statuses_url;

    private String languages_url;

    private String stargazers_url;

    private String contributors_url;

    private String subscribers_url;

    private String subscription_url;

    private String commits_url;

    private String git_commits_url;

    private String comments_url;

    private String issue_comment_url;

    private String contents_url;

    private String compare_url;

    private String merges_url;

    private String archive_url;

    private String downloads_url;

    private String issues_url;

    private String pulls_url;
    
    private boolean privatE;

    private String milestones_url;

    private String notifications_url;

    private String labels_url;

    private String releases_url;

    private String deployments_url;

    private String created_at;

    private String updated_at;

    private String pushed_at;

    private String git_url;

    private String ssh_url;

    private String clone_url;

    private String svn_url;

    private String homepage;

    private int size;

    private int stargazers_count;

    private int watchers_count;

    private String language;

    private boolean has_issues;

    private boolean has_projects;

    private boolean has_downloads;

    private boolean has_wiki;

    private boolean has_pages;

    private int forks_count;

    private String mirror_url;

    private boolean archived;

    private boolean disabled;

    private int open_issues_count;

    private License license;

    private int forks;

    private int open_issues;

    private int watchers;

    private String default_branch;

    private int score;
    
    
    public void addLanguage(String language) {
    	if(this.languages == null) this.languages = new HashSet<String>();
    	
    	this.languages.add(language);
    	
    }
    
    @Override
    public RepositoryData clone() {
        try {
            return (RepositoryData) super.clone();
        } catch (CloneNotSupportedException e) {
        	System.out.println("cannot be cloned");
            return null;
        }
    }
}

