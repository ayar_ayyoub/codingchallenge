package com.challenge.trendrepo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class License implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -2563439838908709364L;

	private String key;

    private String name;

    private String spdx_id;

    private String url;

    private String node_id;

}